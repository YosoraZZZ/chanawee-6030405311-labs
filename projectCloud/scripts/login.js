const auth = firebase.auth();
const signupForm = document.querySelector("#signup-form");
document.getElementById("loginHide").children[2].style.display = "none";
document.getElementById("loginHide").children[3].style.display = "none";
document.getElementById("loginHide").children[5].style.display = "none";

function signupWeb() {
  signupForm.addEventListener("submit", e => {
    e.preventDefault();

    const email = signupForm["signup-email"].value;
    const password = signupForm["signup-password"].value;
    const passwordConfirm = signupForm["signup-password-confirm"].value;
    console.log(email, password);
    if (password != passwordConfirm) {
      alert("Pls check your pasword");
      return false;
    } else {
      auth.createUserWithEmailAndPassword(email, password).then(cred => {
        const modal = document.querySelector("#mymodal");
        signupForm.reset();
        document.getElementById("loginHide").children[4].style.display = "none";
        document.getElementById("loginHide").children[6].style.display = "none";

        document.getElementById("loginHide").children[2].style.display =
          "block";
        document.getElementById("loginHide").children[3].style.display =
          "block";
        document.getElementById("loginHide").children[5].style.display =
          "block";
        $("#myModal").modal("hide");
      });
    }
  });
}
const logout = document.querySelector("#logout");

function logoutWeb() {
  logout.addEventListener("click", e => {
    e.preventDefault();
    auth.signOut().then(() => {
      console.log("user signed out");
      document.getElementById("loginHide").children[4].style.display = "block";
      document.getElementById("loginHide").children[6].style.display = "block";

      document.getElementById("loginHide").children[2].style.display = "none";
      document.getElementById("loginHide").children[3].style.display = "none";
      location.reload();
    });
  });
}

const loginForm = document.querySelector("#login-form");

function loginWeb() {
  loginForm.addEventListener("submit", e => {
    e.preventDefault();

    const email = loginForm["login-email"].value;
    const password = loginForm["login-password"].value;
    console.log(email, password);
    auth
      .signInWithEmailAndPassword(email, password)
      .then(cred => {
        console.log(cred.user);
        loginForm.reset();
        document.getElementById("loginHide").children[4].style.display = "none";
        document.getElementById("loginHide").children[6].style.display = "none";

        document.getElementById("loginHide").children[2].style.display =
          "block";
        document.getElementById("loginHide").children[3].style.display =
          "block";
        document.getElementById("loginHide").children[5].style.display =
          "block";

        $("#myModalLogin").modal("hide");
        sessionStorage.setItem();
      })
      .catch(function (err) {
        console.log(err);
        if (err.code === "auth/wrong-password") {
          alert("รหัสผิด");
        }
      });
  });
}

function resetPassword() {
  const emailAddress = document.getElementById("forgetPass");
  auth
    .sendPasswordResetEmail(emailAddress)
    .then(function () {
      if (emailAddress == "") {
        alert("Please write your email");
        console.log("No write");
      } else {
        alert("Please check your email");
        console.log("Hello");
      }
    })
    .catch(function (error) {});
}

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    console.log(user);
    sessionStorage.setItem("user", JSON.stringify(user));
  } else {
    sessionStorage.setItem("user", null);
  }
});

const disabledUser = document.getElementsByClassName("disabled-user");
const user = JSON.parse(sessionStorage.getItem("user"));

console.log(disabledUser);

Array.from(disabledUser).forEach(element => {
  console.log(element);
  if (user) {
    element.style.display = "none";
    document.getElementById("loginHide").children[2].style.display = "block";
    document.getElementById("loginHide").children[3].style.display = "block";
    document.getElementById("loginHide").children[5].style.display = "block";
  } else {
    element.style.display = "block";
  }
});
console.log(user);