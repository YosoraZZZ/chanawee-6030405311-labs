const firestore = firebase.firestore();
const mainVocab = document.querySelector(".mainVocab");

//mainVocab.innerHTML = "BUTTER";

highScore = document.querySelector("#highScore");
highScore.innerHTML += "000";

score = document.querySelector("#score");
score.innerHTML += "000";

timeLeft = document.querySelector("#timeLeft");
//progressBar = document.querySelector(".progress-bar");

const loadButton = document.querySelector("#startBut");
const leftAns = document.querySelector("#leftAns");
const rightAns = document.querySelector("#rightAns");
var countScore = 0;
var mainWord;
var vocab1 = []; //for mainVocab
var vocab2 = []; //for choice

// Timer //
var timer;
var gammingTime = 5;

function myTimer(sec) {
    $.wait = function (callback, stoptime) {
        return window.setTimeout(callback, stoptime * 1000);
    };

    if (timer) {
        clearInterval(timer);
    }
    var bar = 100;
    timer = setInterval(function () {
        var secdel = sec--;

        $('#timeLeft').text(secdel);
        $('.progress-bar').text(secdel);
        $('.progress-bar').css("width", bar + "%");
        if (sec < 0) {
            clearInterval(timer);

            // $(".gamePlay").hide();
            // $(".gameEnding").show();
        }
        bar -= (100 / gammingTime);
    }, 1000);
    $("#plusx").click(function () {
        sec += 10; //กดปุ่มแล้ว +10 s
    });
}

$(document).ready(function () {
    $(".countStart").click(function () {
        $(".countStart").hide();
        $(".gameStarted").show();
        myTimer(gammingTime);
    });
});

function count3() {
    document.getElementById("countDownPara").innerHTML = "3";
    return countDelay("3")
}

function count2() {
    document.getElementById("countDownPara").innerHTML = "2";
    return countDelay("2")
}

function count1() {
    document.getElementById("countDownPara").innerHTML = "1";
    return countDelay("1")
}

function count0() {
    document.getElementById("countDownPara").style.display = "none";
    return 1;
}

function countDelay(n) {
    var d = $.Deferred();
    setTimeout(function () {
        console.log(n);
        d.resolve();
    }, 1000);
    return d.promise();
}

loadButton.onclick = function () {
    fetchDBtoList();
    count3().pipe(count2).pipe(count1).pipe(count0).pipe(callranVocabAndAns)

    console.log("load")

};

function callranVocabAndAns() {
    ranVocabAndAns(vocab1, vocab2, "EngToTh");
}

function fetchDBtoList() {
    firestore.collection("vocabulary01").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
            vocab1.push(doc.data());
            vocab2.push(doc.data());
        });
        console.log(vocab1);
        console.log("vocab1.length " + vocab1.length);
        console.log(vocab2);
        console.log("vocab2.length " + vocab2.length);
    });
};

function ranVocabAndAns(vocab, ans, lang) {
    do {
        var i = random(vocab.length); //vocab1
        console.log("i = " + i);
        var j = random(ans.length); //vocab2

    }
    while (vocab[i].eng == ans[j].eng); {
        console.log(ans)
        console.log("j = " + j);
        if (lang == "EngToTh") {
            mainVocab.innerHTML = vocab[i].eng;
            mainWord = vocab[i].th; //for check
            swapButton(vocab[i].th, ans[j].th)
            console.log("EngToTh " + vocab[i].eng + " " + vocab[i].th + " " + ans[j].th);
        } else if (lang == "ThToEng") {
            mainVocab.innerHTML = vocab[i].th;
            mainWord = vocab[i].eng; //for check
            swapButton(vocab[i].eng, ans[j].eng)
            console.log("ThToEng " + vocab[i].th + " " + vocab[i].eng + " " + ans[j].eng);
        }
        vocab.splice(vocab.indexOf(vocab[i]), 1);
        console.log(vocab1);
        console.log("vocab1.length after random i : " + vocab1.length);
    }
}

function callchoiceTrueFalseLeft() {
    return choiceTrueFalse(leftAns)
}

function callchoiceTrueFalseRight() {
    return choiceTrueFalse(rightAns)
}

leftAns.onclick = function () {
    console.log("**********letf click************")
    callchoiceTrueFalseLeft()
    ranVocabAndAns(vocab1, vocab2, "EngToTh");

    if (vocab1.length < 1) {
        leftAns.style.display = "none";
        rightAns.style.display = "none";
    }
};
rightAns.onclick = function () {
    console.log("***********Right click***********")
    callchoiceTrueFalseRight();
    ranVocabAndAns(vocab1, vocab2, "EngToTh");

    if (vocab1.length < 1) {
        leftAns.style.display = "none";
        rightAns.style.display = "none";
    }
};

function choiceTrueFalse(ansBut) {
    console.log("mainVocab : " + mainVocab.innerHTML);
    console.log(mainWord)
    console.log("ansBut: " + ansBut.innerText);
    if (ansBut.innerText == mainWord) {

        countScore += 1
        score.innerHTML = countScore;
        console.log("true");
    } else {
        console.log("wrong");
    }
}

//สลับปุ่มซ้าย-ขวา
function swapButton(a, b) {
    var i = random(2);
    if (i == 0) {
        leftAns.innerText = a;
        rightAns.innerText = b;
    }
    if (i == 1) {
        leftAns.innerText = b;
        rightAns.innerText = a;
    }
}

function random(number) {
    return Math.floor(Math.random() * number);
}