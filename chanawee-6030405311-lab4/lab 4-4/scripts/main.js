var lastResult = document.querySelector('.lastResult');
var generateRandom = document.querySelector('.generateRandom');

function randomAndShow(){
    
    var adjective_Thai = ["เป็นคนดี", "เป็นโปเกมอนเทรนเนอร์ที่ดี", "เป็นคนไม่โกง", "เป็นนักวิ่งที่ดี", "เป็นนายกที่ดี"]
    var adjective_Eng = ["is good boy", "is good Pokemon's trainer", "is good honest", "is good president", "is good runner"]
    var type = document.getElementsByName("language");
    var random;
    var x = document.getElementById("myText").value;

   
    
    if(type[0].checked){
        random = adjective_Eng[Math.floor(Math.random() * adjective_Eng.length)];
        
        lastResult.textContent =  x + ' ' + random;
        lastResult.style.backgroundColor = 'yellow';
    }

    else{
        random = adjective_Thai[Math.floor(Math.random() * adjective_Thai.length)];
        
        lastResult.textContent = x + ' ' + random;
        lastResult.style.backgroundColor = 'yellow';
    }
    return random, lastResult, x;
}
